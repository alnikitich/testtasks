const fs       = require('fs');
const zlib     = require('zlib');
const gzipme   = require('gzipme');
var startDate = new Date("2017-11-14T00:00:00.000Z");
var deltaTime = 40000;

var companies = generateCompains(10);
var events = ["click","show","other"];

var fileName = `../logs/pid-${parseInt(Math.random()*1000)}.log`;

createFile(fileName);
gzipme(fileName);
fs.unlinkSync(fileName);

function generateCompains(count){
	var res = [];
	for (var i=0; i<count; i++){
		res.push("compain" + i);
	}
	return res;
}


function createFile(fileId){

	var file = fs.openSync(fileId, "w");

	for (i=0; i<30000; i++){

		var line = {
			time: (new Date(startDate.getTime() + deltaTime * i)).toISOString(),
			campaign_id: companies[ parseInt (Math.random() * (companies.length - 1)) ],
			event_type:	 events[ parseInt (Math.random() * (events.length - 1)) ]
		}

		fs.writeSync(file, JSON.stringify(line)+"\r\n");
	}

	fs.closeSync(file);
}

