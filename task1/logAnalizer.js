'use strict';

const fs        = require('fs');
const zlib      = require('zlib');
const readline  = require('readline');
const path 		= require('path');
var   cacheStorage;

// ------------------------------------------------------------------------------------------
// main function 
function analize(config, query){

	cacheStorage = new CacheStorage();

	// at start precache all new log files
	let files2PreAnalize = getNonAnalizedFilesList(config.srcPath, config.destPath);
	if (!files2PreAnalize.length) return analizeFromCache();

	var counter = 1;
	files2PreAnalize.forEach(function(item){
		var filePath = path.join(config.srcPath,item.id);
		var data = analizeSingleFile(filePath, "../", function(results){
			cacheStorage.writeCacheFile(results, item.id, item.time);

			if (counter++ >= files2PreAnalize.length)
				analizeFromCache();
		});
	});

	// calc sttatistics based on cache files
	function analizeFromCache(){
		var res = calcAnalizedPeriod(config, query);
		for (var k in res)
			stdout(k + ":", res[k])
	}
}

// ------------------------------------------------------------------------------------------
// analize all non exist in destPath files
// analize all changed files (date should be more new)
function getNonAnalizedFilesList(srcPath, destPath){

		var res = [];
		var cacheInfo = cacheStorage.getCacheInfo();
		var filesList = fs.readdirSync(srcPath);
		cacheStorage.clearRemovedFiles(filesList);

		filesList.forEach(function(item){

			try{
				var fileStat = fs.statSync(path.join(srcPath,item));

				// add -> if it`s a new file or file modification time != analized file previosly
				if (!cacheInfo[item] || cacheInfo[item] !== fileStat.mtime.toISOString())
					res.push ({id:item, time: fileStat.mtime});
			}
			catch(ex){
				stderr("file",item, "access error, Skipped");
				stderr(ex);
			}

		});

		return res;
}

var plist = ["time","campaign_id","event_type"];
var event_type_value = "show";

function analizeSingleFile(srcFile,destPath,cb){
	var counters = {};

	try{
		let pipe = fs.createReadStream(srcFile).pipe(zlib.createGunzip())

		let lineReader = readline.createInterface({
			input: pipe
		});

		pipe.on("error", function(err){
			stderr (err);
		})

		let n = 0;
		lineReader.on('line', (line) => {
			n += 1
			if (n / 1000000 == Math.round(n/1000000))
				stderr("line: " + n);
			
			if (!line.trim().length)
				return;

			try{ 	var cobject = JSON.parse(line);   }
			catch(ex){
				stderr("Parse error. Line:", n,srcFile, line);
				return;
			}

			if (!hasAllParameters(cobject)){
				stderr("Parse error. Line",n,"has wrong format", srcFile, line);
				return;
			}

			if (cobject["event_type"] === event_type_value){

				let date = new Date(cobject["time"]);

				if (isNaN( date )){
					stderr("Parse error. Date has wrong fomat. Line", n,srcFile, line);
					return;
				}

				date.setUTCHours(0,0,0,0);

				date = date.toISOString();
				if (!counters[date]) counters[date] = {};
				if (!counters[date][cobject["campaign_id"]]) counters[date][cobject["campaign_id"]] = 0;
				counters[date][cobject["campaign_id"]] += 1;
			}
		});

		lineReader.on('close', function(){
			if (cb) cb(counters);
		})

	}
	catch(ex){
		stderr(ex);
	}

	function hasAllParameters(item){
		for (var i=0; i<plist.length; i++)
			if (!item[plist[i]]) return false;

		return true;
	}
}

// -------------------------------------------------------------------------
function calcAnalizedPeriod(config, query){

	var filesList = cacheStorage.getFilesList();
	var res = {};
	
	filesList.forEach(function(fileId){
		let content = cacheStorage.getContent(fileId);
		if (!content) return;

		let cDate = new Date(query.startDate);

		while (cDate <= query.endDate){

			let campaigns = content[cDate.toISOString()];
			for (var k in campaigns){
				res[k] = res[k] ? res[k]+campaigns[k] : campaigns[k];
			}

			cDate.setTime( cDate.getTime() + 86400000 ); // go to next day
		}
	});	

	return res;
}

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// cache storage
// store every log analize result 
// remova results from cache if log file was delete
class CacheStorage{
	constructor(cacheFileId = "cacheInfo.json"){
		this.cachePath = this.getHomeDir();
		this.cacheFileId = cacheFileId;

		stderr(this.cachePath ,"used for cache store");
	}
}

CacheStorage.prototype.writeCacheFile = function(data, fileId, fileUpdateTime){
	try{
		var cacheFileId = fileId.replace(/\.[^/.]+$/, ".json");
		fs.writeFileSync(path.join(this.cachePath, cacheFileId), JSON.stringify(data));
		this.updateCacheInfo(fileId, fileUpdateTime);
	}
	catch(ex){
		stderr("file:",fileId,"can not be saved to ",this.cachePath);
		stderr("reason:",ex);
		process.exit(1);
	}
}

CacheStorage.prototype.setCachePath = function(cachePath) {_cachePath = cachePath;}

CacheStorage.prototype.getCacheInfo = function(cachePath){
	try{
		return JSON.parse(fs.readFileSync(path.join(this.cachePath, this.cacheFileId)));
	}
	catch(ex){
		return {};
	}
}

CacheStorage.prototype.updateCacheInfo = function(fileId, fileUpdateTime){
	if (!fs.existsSync(this.cachePath)) fs.mkdir(this.cachePath);

	var cacheInfo = this.getCacheInfo();
	cacheInfo[fileId] = fileUpdateTime.toISOString();
	fs.writeFileSync(path.join(this.cachePath, this.cacheFileId),JSON.stringify(cacheInfo),()=>{});
}

CacheStorage.prototype.getFilesList = function(fileId, fileUpdateTime){
	return fs.readdirSync(this.cachePath);
}

CacheStorage.prototype.getContent = function(fileId){
	try{
		return JSON.parse(fs.readFileSync(path.join(this.cachePath, fileId)));
	}
	catch(ex){
		return false;
	}

}

CacheStorage.prototype.clearRemovedFiles = function(logFiles){
	var tmpLogFilesObj = {}
	for (var i=0; i<logFiles.length; i++) tmpLogFilesObj[logFiles[i].replace(/\.[^/.]+$/, ".json")] = 1;

	var cacheFiles = fs.readdirSync(this.cachePath);
	for (i=0; i<cacheFiles.length; i++) {
		if (cacheFiles[i] == this.cacheFileId) continue;
		if (!tmpLogFilesObj[cacheFiles[i]])
			fs.unlinkSync(path.join(this.cachePath, cacheFiles[i]));
	}
}

CacheStorage.prototype.getHomeDir = function(logFiles){
	let home;
	
	switch (process.platform) {
		case "win32":
		case "win64":
			home = process.env.LOCALAPPDATA || process.env.APPDATA;
			if (!home) { throw new Error("Couldn't find the base application data folder"); }
			home = path.join(home, ".testCache");
			break;
		case "linux":
			home = process.env.HOME;
			if (!home) { throw new Error("Couldn't find the base application data directory"); }
			home = path.join(home, ".testCache");
			break;
		default:
			throw new Error("Can't use the Node Webkit relative path for platform " + process.platform);
			break;
	}


	if (!fs.existsSync(home)) fs.mkdirSync(home)

	return home;
}


// -----------------------------------------------------------------------------------------------------------
module.exports = analize;

