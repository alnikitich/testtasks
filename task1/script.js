//code for node.js 8.0+ (ES6)

const fs 		= require("fs");
const analizer 	= require("./logAnalizer");

let [config, query] = init();

if (!config) 	showHelp();
else			execute(config, query);

// ----------------------------------------------------------------
// ----------------------------------------------------------------

function init(){

// globals out function
	global.stdout  = function (){ console.log.apply   (undefined,arguments);}
	global.stderr  = function (){ console.error.apply (undefined,arguments); }
	global.stdinfo = function (){ console.error.apply (undefined,arguments); }

// read and parse config file
	const configFilePath = "./config.json";
	let query = {}, config;

	try{		config = fs.readFileSync(configFilePath); }
	catch(ex){	stderr("Error in open config file:",configFilePath); return [false,false];	}

	try{		config = JSON.parse(config); }
	catch(ex){	stderr("Error parse config file (wrong JSON):"); return [false,false];	}

	try{		var exist = fs.existsSync(config.srcPath); 
				if (!exist) {
					stderr(`Error: source directory ${config.srcPath} is not exist.`); 
					return [false,false];
				}
	}	
	catch(ex){	stderr(`Error: source directory ${config.srcPath} is not exist.`); return [false,false];	}

// read command line parameters

	// todo date1 < date2 !!!

	if (!process.argv[2] || !process.argv[3]) return [false,false];
	if (!(query.startDate = checkParameter(process.argv[2]))) return [false,false];
	if (!(query.endDate   = checkParameter(process.argv[3]))) return [false,false];
	
	function checkParameter(value){
		let res = new Date(value);
		if (isNaN( res )){
			stderr ("Error: command line parameter value",value,"is incorrect");
			return false;
		}
		return res;
	}

	return [config, query];
}

function showHelp(){
	const br = "\r\n";
	stderr (br+"Usage: node script \"start date in ISO format\" \"end date in ISO format\"");
	stderr (br+"Example: node script.js \"2017-11-14T00:00:00.000Z\" \"2017-11-15T00:00:00.000Z\"");
}

function execute(config, query){
	analizer(config, query);
}
