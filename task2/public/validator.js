function checkEmptyFields(fields){
	for (var i=0; i<fields.length; i++){
		var el = document.getElementsByName(fields[i])[0];
		if (!el) return false;

		if (el.value === null || el.value === undefined || !el.value.length){
			alert("filed " + fields[i] + " can`not be empty");
			return false;
		}
	}

	return true;
}