const express 		= require('express');
const bodyParser 	= require('body-parser');
const pug 			= require('pug');
const path   		= require('path');
const url 			= require('url');
const querystring   = require('querystring');
const mongoId   	= require('mongodb').ObjectID;

// ----------------------------------------------------------------------------------------------------------
// objects config descriptors

var campainFields = {
	"name":{required:true},
	"active":{setter:(value)=>{return value ? true : false}},
	"creationDate":{updated:false,default:()=>new Date()}};

var bannerFields  = {
	"campainId":{required:true},
	"name":{required:true},
	"active":{setter:(value)=>{return value ? true : false}},
	"creationDate":{updated:false, default:()=>new Date()},
	"options":{required:true, default:"{settings:{}}"}}; 

// ----------------------------------------------------------------------------------------------------------
// init backEnd function
module.exports = (httpPort, mongoBridge) => {

	app = express();
	app.set('view engine', 'pug')
	app.set('views', './views')
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));
	app.use('/www',express.static('public'));
	app.locals.basedir = path.join(__dirname, 'public');

	app.get("/",function(req,res){
		res.redirect("/view/campain");
	});

	// show campains list
	app.get("/view/campain",function(req,res){
		mongoBridge.queryData("campain",{},{name:1},false)
		.then (result => res.render('campainList',{data:result}))
		.catch(error => processError(req, res, error));
	});

	// show custom pug page
	app.get("/view/custom/:viewId",function(req,res){	
		res.render(req.params.viewId, Object.assign(req.query,req.body));
	});

	// update campain
	app.post("/api/campain/:id", function(req,res){
		let params = getQ(req);
		let [prErr, storedObj] = prepareObject4Publish (campainFields, params);
		if (prErr) return processError(req, res, prErr);

		var mongoCampaind = new mongoId(params.id);
		mongoBridge.queryData("campain",{_id: mongoCampaind},{},true)
		.then(storedCompainDoc => {
			if (storedCompainDoc.active !== storedObj.active) // update banners status
				return mongoBridge.updateObject("banner", {"campainId" : mongoCampaind}, {$set:{active:storedObj.active}}, true);
			return true; 
		})
		.then(storedCompainDoc => mongoBridge.updateObject("campain", {_id: mongoCampaind}, {$set:storedObj}, false))
		.then(()=>res.redirect(addQueryParameters2URL("/view/campain/"+params.id,{notifyMessage:"Campain successfully updated"})))
		.catch(error=> processError(req, res, error));
	});

	// update banner
	app.post("/api/campain/:campainId/banner/:id", function(req,res){
		let params = getQ(req);
		let [prErr, storedObj] = prepareObject4Publish (bannerFields, params);
		if (prErr) return processError(req, res, prErr);

		storedObj.campainId = new mongoId(storedObj.campainId);

		mongoBridge.updateObject("banner", {_id: new mongoId(params.id)}, {$set:storedObj}, false)
		.then(()=>mongoBridge.queryDataCount("banner",{campainId:storedObj.campainId,active:true}) )
		.then(activeBannesCount=>{
			if (!activeBannesCount) // no active banners in campain
				return mongoBridge.updateObject("campain", {_id: storedObj.campainId}, {$set:{active:false}}, false);
			return true;
		})
		.then(result=>res.redirect(addQueryParameters2URL("/view/campain/" + params.campainId +"/banner/" +params.id,
															{notifyMessage:"Banner successfully updated"})))
		.catch(error=>processError(req, res, error));
	});

	// show banner edit page
	app.get("/view/campain/:campainId/banner/:bannerId", function(req,res){
		let params = getQ(req);
		Promise.all([
			mongoBridge.queryData("banner",{_id:new mongoId(params.bannerId)},{},true),
			mongoBridge.queryData("campain",{_id:new mongoId(params.campainId)},{},true)
		])
		.then(results => {
			var response = results[0] || {};
			response.params = params;
			if (results[1]){
				response.campainName   = results[1].name;
				response.campainActive = results[1].active;
			}
			res.render('editBanner',response);
		})
		.catch(error => processError(req, res, error))
	});

	// show campain edit page
	app.get("/view/campain/:id", function(req,res){
		let params = getQ(req);

		Promise.all([ 
			mongoBridge.queryData("campain",{_id:new mongoId(params.id)},{},true),
			mongoBridge.queryData("banner",{campainId:new mongoId(params.id)},{name:1},false) ])
		.then(results => {
			if (!results[0]) results[0] = {};
			results[0].banners = results[1]; 
			results[0].params  = params;
			res.render('editCampain',results[0]);
		})
		.catch(error => processError(req, res, err));
	});

	// insert new banner
	app.post("/api/campain/:campainId/banner", function(req,res){
		var params = getQ(req);
		params.campainId = new mongoId(params.campainId);
		var [prErr, storedObj] = prepareObject4Publish (bannerFields, params, true);
		if (prErr) return processError(req, res, prErr);
		storedObj.creationDate = new Date();
		storedObj.active = false;

		mongoBridge.createMongoObject("banner", storedObj)
		.then(() => res.redirect("/view/campain/"+params.campainId))
		.catch(error => processError(req, res, error));
	});

	// query random campain banner
	app.get("/api/campain/:id/randomBanner", function(req,res){
		var params = getQ(req);

		mongoBridge.getRandomCampainBanner(params.id)
		.then(banner=>{
			if (!banner) return res.send({});
			else return res.send({
				"banner_id": banner._id,
				"settings":  banner.options
			});
		})
		.catch(error => processError(req, res, error))
	});

	// create new campain
	app.post("/api/campain", function(req,res){
		let params = getQ(req);
		let [prErr, storedObj] = prepareObject4Publish (campainFields, params, true);		
		if (prErr) return processError(req, res, prErr);
		storedObj.creationDate = new Date();
		storedObj.active = false;

		mongoBridge.createMongoObject("campain", storedObj)
		.then(() => res.redirect("/"))
		.catch(error => processError(req, res, error));
	});

	/*	if err -> redirect to current page with error message
		if !err  */
	function addQueryParameters2URL(_url, queryData){
		let curUrl = url.parse(_url);
		let query  = querystring.parse(curUrl.query);
		
		for (var k in queryData)
			query[k] = queryData[k];
				
		curUrl.query = querystring.stringify(query);
		curUrl.search = "?" + curUrl.query;
		return url.format(curUrl);
	}

	function processError(req, res, error){
		return res.redirect(addQueryParameters2URL(req.get('referer'),{notifyMessage:error}));
	}

	function getQ(req){
		return Object.assign(req.query, req.params, req.body);
	}

	// prepare object for save to mongo storage 
	// - remove locked (read only) fields
	// - set default values (on first object create)
	// - check for required fuields
	function prepareObject4Publish(objectFields, data, createObject){
		var res = {};
		var fields = Object.keys(objectFields);
		for (var i=0; i<fields.length; i++){
			let value = data[fields[i]];

			if (objectFields[fields[i]].setter)
				value = objectFields[fields[i]].setter(data[fields[i]]);

			if (createObject && !value && objectFields[fields[i]].default)
				if (typeof objectFields[fields[i]].default == "function")
					value = objectFields[fields[i]].default();
				else
					value = objectFields[fields[i]].default;

			// skip fields with updated=false flag (lock it) 
			if (objectFields[fields[i]].updated === false)
				continue;

			if (objectFields[fields[i]].required && (value===null || value===undefined || value.length == 0))
				return ["Error: field " + fields[i] +" can`not be empty.",undefined];

			res[fields[i]] = value;
		}

		return [undefined,res];
	}

	// start backEnd
	app.listen(httpPort, function () {
		console.log(`Server was started on ${httpPort} port.`);
	});

	return true;
}
