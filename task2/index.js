const fs 		  = require("fs");
const mongoBridge = require("./mongoBridge");
const backend 	  = require("./backEnd");
const config  	  = JSON.parse(fs.readFileSync("./config.json","utf8"));

mongoBridge.init(config.mongoURL)
	.then(()=>{
		backend(config.httpPort, mongoBridge)
	})
	.catch(error => {
		console.error(error);
		process.exit(1);
	});

