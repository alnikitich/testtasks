const MongoClient   = require('mongodb').MongoClient;
const mongoId   	= require('mongodb').ObjectID;

exports.init = (mongoURL) => {
	return MongoClient.connect(mongoURL)
	.then( (db) => {
		dbConnection = db.db("myproject");
		initDBStructire();
		return true;
	});

	function initDBStructire(){
		var col_campain = dbConnection.collection('campain');
		var col_banner  = dbConnection.collection('banner');

		col_banner.createIndex({ campainId : 1 });	
		col_banner.createIndex({ active : 1 });	
	}
}

exports.createMongoObject = (collectionId, data) => {
	return dbConnection.collection(collectionId).insertOne(data);
}

exports.queryData = (collectionId,query,sort,findOne) => {
	return dbConnection.collection(collectionId).find(query).sort(sort).toArray()
	.then( (result) => findOne ? result[0] : result);
}

exports.queryDataCount = (collectionId,query) => {
	return dbConnection.collection(collectionId).find(query).count();
}

exports.updateObject = (collectionId, query ,object,multiDoc) => {
	return dbConnection.collection(collectionId).update(query, object, {"multi": multiDoc});
}

exports.getRandomCampainBanner = (campainId)=> {
	return dbConnection.collection("banner")
	.aggregate([{$match: {campainId: new mongoId(campainId), active:true}}, {$sample: { size: 1 }}]).toArray()
	.then(result=>result[0]);
}
